<?php

namespace App\Http\Controllers;

use App\FeedBack;
use phpQuery ;
class WeatherController extends Controller
{
    public function getContents (){

    }

    public function show()
    {
        {
            $html = file_get_contents('http://www.gismeteo.ua/city/daily/5093/');
             $pq= phpQuery::newDocument($html);
            $query =  $pq->find('.date');

            foreach ($query as $date){
                $dates [] = trim(pq($date)->text());
            }
            $query = $pq->find('.tab-weather__value_l');
            foreach ($query as $item){
                $nowtemp[] = trim(pq($item)->text());
            }
            $query = $pq->find('body > 
            section > 
            div.content_wrap > 
            div > 
            div.main > 
            div > 
            div.__frame_sm > 
            div.forecast_frame.hw_wrap > 
            div.tabs._center > div > div > 
            div.tab-content > 
            div.tabtempline.tabtemp_0line.clearfix > 
            div > 
            div > 
            div > 
            div > 
            span.unit.unit_temperature_c');
                  foreach ($query as $item){
                $todaytemp[] =trim(pq($item)->text());
            }
            $query = $pq->find('body > 
            section > 
            div.content_wrap > 
            div > 
            div.main > 
            div > 
            div.__frame_sm > 
            div.forecast_frame.hw_wrap > 
            div.tabs._center > 
            a.nolink.tab.tablink.tooltip > 
            div > div.tab-content > 
            div.tabtempline.tabtemp_1line.clearfix > 
            div > 
            div > 
            div > 
            div > 
            span.unit.unit_temperature_c');
            foreach ($query as $item) {
                $tomorrowtemp[] = trim(pq($item)->text());
            }
            phpQuery::unloadDocuments();

            $feedbackarray = FeedBack::all()->toArray();
          return view('weather',[
              'feedbackarray'=>$feedbackarray,
              'dates'=>$dates,
              'nowtemp'=>$nowtemp,
              'todaytemp' =>$todaytemp,
              'tomorrowtemp'=>$tomorrowtemp
          ]);

        }
    }
}

