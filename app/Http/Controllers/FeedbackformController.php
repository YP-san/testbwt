<?php

namespace App\Http\Controllers;

use App\Http\Requests\feedbackformRequest;
use App\FeedBack;

class FeedbackformController extends Controller
{
    public function show()
    {
        return view('feedbackform');
    }


    public function store(feedbackformRequest $request)
    {
            $qrystr = $request->post();
            $feedback = new FeedBack;
            $feedback->name = $qrystr['name'];
            $feedback->email = $qrystr['email'];
            $feedback->message = $qrystr['message'];
            $feedback->save();
            return redirect('feedbackform');
        }

}
