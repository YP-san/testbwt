<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedBack extends Model
{
        protected $table = 'feedbacks';
        public $primaryKey = 'id';
        public $timestamps = TRUE;
}
