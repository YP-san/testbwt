
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Simple weather parser</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">
</head>

<body>
@section('header')
 <header class="container">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="{{route('weather')}}">Simple weather parser</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('weather')}}">Weather <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('feedbackform')}}">Feedback</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{route('mainpage')}}">Welcome page</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
@endsection @yield('header')

   @section('content')<main role="main" style="margin-top: 8vh">
            <div class="jumbotron" style="margin-bottom: 2vh;margin-top: 10vh">
                <div class="container"style="">
                    <h3 style="text-align: center ;margin-bottom: 5vh">Weather in ZP</h3>
                    <div class="row">
                        @if(!empty($dates))
                            <div class="col-md-4" style="">
                                <h4>{{$dates[0]}}</h4>
                                <h5>{{$dates[1]}}</h5>
                                <p>Температура  {{$nowtemp[0]}} °C</p>
                                <p>Fahrenheit  {{$nowtemp[1]}} °F</p>
                                <hr>
                            </div>
                        <div class="col-md-4" style="">
                            <h4>{{$dates[2]}}</h4>
                            <h5>{{$dates[3]}}</h5>
                            <p>Температура от {{$todaytemp[0]}} °C</p>
                            <p>До{{$todaytemp[1]}} °C </p>
                            <hr>
                        </div>
                        <div class="col-md-4" style="">
                            <h4>{{$dates[4]}}</h4>
                            <h5>{{$dates[5]}}</h5>
                            <p>Температура  {{$tomorrowtemp[0]}} °C</p>
                            <p>До  {{$tomorrowtemp[1]}} °С</p>
                            <hr>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="container"
                 style="padding:0 5vw 3vh;
            margin-bottom: 2vh">
                <h2 style="text-align: center;margin-bottom: 5vh">Feedbacks</h2>
                <div class="row">
                    @if (!empty($feedbackarray))
                @foreach($feedbackarray as $key=>$value)
                            <div class="col-md-4">
                            <h4>{{$value['name']}}</h4>
                                <p>{{$value['message']}}</p>
                                <hr>
                            </div>
                    @endforeach
                        @endif
                </div>
            </div>
        </main>@endsection @yield('content')

    @section('footer')   <footer class="container">
            <p>&copy; Simple weather parser 2019 by YP</p>
        </footer>@endsection @yield('footer')

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    </body>
</html>
